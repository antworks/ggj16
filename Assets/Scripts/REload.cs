﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class REload : MonoBehaviour
{
	public Text text;

	private void Start()
	{
		text.DOFade (1f, 1f);
	}

	public void Click()
	{
		SceneManager.LoadScene ("Menu");
	}
}
