﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public struct PreachMapper
{
	public string preachId;
	[Range(-1, 1)]public int[] neededEvents;
}

[System.Serializable]
public class Act
{
	[HideInInspector]
	public List<PreachMapper> preachMapper = new List<PreachMapper>();

	public List<Preach> GetCurrentPreaches()
	{
		List<Preach> matchedPreaches = new List<Preach>();

		for (int i = 0; i < preachMapper.Count; i++)
		{
			if (MatchEvents(preachMapper[i]))
			{
				matchedPreaches.Add(PreachMaster.GetPreach(preachMapper[i].preachId));
			}
		}

		return matchedPreaches;
	}

	public bool MatchEvents(PreachMapper preachToMatch)
	{
		for (int i = 0; i < GameMaster.NUM_OF_EVENTS; i++)
		{
			if (preachToMatch.neededEvents[i] != 0 && preachToMatch.neededEvents[i] != GameMaster.events[i])
			{
				return false;
			}
		}

		return true;
	}
}