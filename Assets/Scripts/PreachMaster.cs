﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PreachMaster : MonoBehaviour 
{
	public const string PREACHERS_PATH = "Preachers/";

	public static PreachMaster instance;

	public List<Preach> preaches;
	private bool onPreach;

	public PreacherMaster preacherMaster;
	
	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(gameObject);
			return;
		}
	}

	public static Preach GetPreach(string id)
	{
		for (int i = 0; i < instance.preaches.Count; i++)
		{
			if (instance.preaches[i].id == id)
			{
				return instance.preaches[i];
			}
		}

		Debug.LogError("id not found: " + id);
		return null;
	}

	[ContextMenu("LoadPreachers")]
	private void LoadPreachers()
	{
		preaches = new List<Preach>();

		TextAsset[] preachersFiles = Resources.LoadAll<TextAsset>(PREACHERS_PATH);

		for (int i = 0; i < preachersFiles.Length; i++)
		{
			LoadSinglePreacher(preachersFiles[i]);
		}
	}

	private Preacher LoadSinglePreacher(TextAsset preacherFile)
	{
		Preacher currentPreacher;
		Preach newPreach;
	
		int preacherIndex = preacherMaster.GetPreacherIntByName (preacherFile.name);
		if (preacherIndex == -1)
		{
			currentPreacher = new Preacher ();
			currentPreacher.name = preacherFile.name;
		}
		else
		{
			currentPreacher = preacherMaster.preachers [preacherIndex];
		}

		onPreach = false;

		string[] fileLines = preacherFile.text.Split('\n');
		newPreach = new Preach();
		newPreach.eventToChange = -1;

		for (int i = 0; i < fileLines.Length; i++)
		{
			string line = fileLines[i].Trim();

			if (line == "{")
			{
				if (onPreach)
				{
					Debug.LogError("Found '{' on preach. Line: " + (i + 1).ToString() + ". Archive: " + preacherFile.name);
				}
				else
				{
					newPreach = new Preach();
					newPreach.preacher = currentPreacher;
					onPreach = true;
				}
				continue;
			}
			else if (line == "}")
			{
				if (onPreach)
				{
					preaches.Add(newPreach);
					onPreach = false;
				}
				else
				{
					Debug.LogError("Found '}' while not on preach. Line: " + (i + 1).ToString());
				}
				continue;
			}
			else if (onPreach)
			{
				string[] substrings = line.Split(':');

				switch(substrings[0].Trim())
				{
				case "id":
					newPreach.id = substrings[1].Trim();
					break;
				case "[":
					do 
					{
						i++;
						if (fileLines[i].Trim() != "]")
						{
							newPreach.text += fileLines[i] + "\n";
						}
					}
					while (fileLines[i].Trim() != "]");
					break;
				case "tense":
					newPreach.isTense = true;
					break;
				case "eventToChange":
					newPreach.eventToChange = int.Parse (substrings [1]);
					break;
				default:
					Debug.LogError("What the fuck?! Line: " + (i + 1).ToString() + "; document: " + preacherFile.name);
					break;
				}
			}
		}

		return currentPreacher;
	}
}