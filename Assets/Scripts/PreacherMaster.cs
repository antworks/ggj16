﻿using UnityEngine;
using System.Collections.Generic;

public class PreacherMaster : MonoBehaviour
{
	public List<Preacher> preachers = new List<Preacher>();

	public int GetPreacherIntByName(string name)
	{
		for (int i = 0; i < preachers.Count; i++)
		{
			if (preachers [i].name.Equals (name, System.StringComparison.OrdinalIgnoreCase))
			{
				return i;
			}
		}
		return -1;
	}
}
