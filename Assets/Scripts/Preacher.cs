﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Preacher
{
	public string name;
	public Sprite sprite;
	public int soundtrackIndex;
}