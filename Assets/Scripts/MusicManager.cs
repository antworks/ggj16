﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicManager : MonoBehaviour {

	public static MusicManager instance;

	private AudioSource audioPlayer;

	public AudioSource[] tracks;
	public AudioSource quantumTrack;

	public AudioClip quantumPiece;
	public AudioClip clearLine;

	private List<int> offTracksIndexes = new List<int>() { 0, 1, 2, 3 };
	private List<int> onTracksIndexes = new List<int>();

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy (gameObject);
			return;
		}

		StartRandomTrack();
		audioPlayer = GetComponent<AudioSource>();
	}

	[ContextMenu("Play quantum piece")]
	public void PlayQuantumPiece()
	{
		audioPlayer.PlayOneShot(quantumPiece);
	}

	[ContextMenu("Play clear line")]
	public void PlayClearLine()
	{
		audioPlayer.PlayOneShot(clearLine);
	}

	[ContextMenu("Start quantum track")]
	public void StartQuantumTrack()
	{
		quantumTrack.Play();
	}

	[ContextMenu("Stop quantum track")]
	public void StopQuantumTrack()
	{
		quantumTrack.Stop();
	}

	[ContextMenu("Start random track")]
	public void StartRandomTrack()
	{
		if (offTracksIndexes.Count == 0)
		{
			return;
		}

		int rndIndex = offTracksIndexes[Random.Range(0, offTracksIndexes.Count)];
		offTracksIndexes.Remove(rndIndex);
		onTracksIndexes.Add(rndIndex);
		StartCoroutine(SmoothVolumeTo(rndIndex, 1f));
	}

	[ContextMenu("Stop random track")]
	public void StopRandomTrack()
	{
		if (onTracksIndexes.Count == 0)
		{
			return;
		}

		int rndIndex = onTracksIndexes[Random.Range(0, onTracksIndexes.Count)];
		onTracksIndexes.Remove(rndIndex);
		offTracksIndexes.Add(rndIndex);
		StartCoroutine(SmoothVolumeTo(rndIndex, 0f));
	}

	[ContextMenu("Stop all but one track")]
	public void StopAllButOneTrack()
	{
		int playingTracks = onTracksIndexes.Count;
		for (int i = 0; i < playingTracks - 1; i++)
		{
			StopRandomTrack();
		}
	}

	private IEnumerator SmoothVolumeTo (int rndIndex, float targetVolume)
	{
		float delta = targetVolume - tracks[rndIndex].volume;

		while (tracks[rndIndex].volume != targetVolume)
		{
			if (delta > 0)
			{
				tracks[rndIndex].volume = Mathf.Min(targetVolume, tracks[rndIndex].volume + delta * Time.deltaTime);
			}
			else
			{
				tracks[rndIndex].volume = Mathf.Max(targetVolume, tracks[rndIndex].volume + delta * Time.deltaTime);
			}

			yield return new WaitForEndOfFrame();
		}
	}
}