﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class PrayLevel : MonoBehaviour
{
	public static PrayLevel Instance = null;

	public ActsMaster actsMaster;
	public Act currentAct = null;
	private List<Preach> preaches = new List<Preach>();

	public Transform vortexImage;
	public GameObject preachButtonsParent;
	public List<PrayButton> preachButtons = new List<PrayButton>();
	private int[] preachState;
	private int selectedPreach;

	public Text text;

	public JudgementButton conceedButton, denyButton, receivedButton;
	public JudgementButton confirmButton;

	public int testPrayCount = 3;
	public int availableMiracles = 2;

	public Animator vortexAnimator;

	public GameObject[] miracles;
	public GameObject vortexMiracle;

	public Image textBox, scroll;
	public Image actImage;
	public Sprite[] actSprites;
	private int actSpriteIndex = 0;

	private void Awake()
	{
		if (Instance == null)
			Instance = this;
	}

	private IEnumerator Start()
	{
		Fade (1f, 0.5f);
		actImage.DOFade (1f, 0.5f);
		yield return new WaitForSeconds (0.5f);
		actImage.DOFade (0f, 0.5f);
		StartAct ();

		yield break;
	}

	private void StartAct()
	{
		if (currentAct == null)
			return;

		preaches = ActsMaster.GetCurrentPreaches();
		preachState = new int[preaches.Count];
		confirmButton.gameObject.SetActive(false);
		miracles[0].GetComponent<MiracleMover>().FadeIn();
		miracles[1].GetComponent<MiracleMover>().FadeIn();
		availableMiracles = 2;

		vortexAnimator.SetTrigger ("Open");

		SetPreaches (preaches);
		SelectPreach (0);
	}

	private void EndAct()
	{
		for (int i = 0; i < preaches.Count; i++)
		{
			Preach preach = preaches[i];
			if (preach.eventToChange != -1)
			{
				GameMaster.events[preach.eventToChange] = preachState[i];
			}
		}
			
		preaches = null;

		vortexAnimator.SetTrigger ("Close");

		actImage.sprite = actSprites [++actSpriteIndex % actSprites.Length];
		Fade (0f, 0.5f);
		actImage.DOFade (1f, 0.5f);

		conceedButton.ChangeState(JudgementButton.ButtonState.normal);
		denyButton.ChangeState(JudgementButton.ButtonState.normal);
	}

	private void Fade(float a, float t)
	{
		text.DOFade (a, t);
		textBox.DOFade (a, t);
		scroll.gameObject.SetActive ((a > 0.5f) ? true : false);

		conceedButton.GetComponent<Image> ().DOFade (a, t);
		denyButton.GetComponent<Image> ().DOFade (a, t);
		receivedButton.GetComponent<Image> ().DOFade (a, t);
		confirmButton.GetComponent<Image> ().DOFade (a, t);

		preachButtonsParent.SetActive ((a > 0.5f) ? true : false);
	}

	[ContextMenu("Test act end")]
	public void TransitionAct()
	{
		EndAct();
		ActsMaster.currentAct++;
		if (ActsMaster.currentAct >= GameMaster.NUM_OF_ACTS)
		{
			Debug.Log ("GAME OVER");
			confirmButton.gameObject.SetActive(false);
			SceneManager.LoadScene ("End");
		}
		else
		{
			StartCoroutine (StartActCoroutine ());
		}
	}

	private IEnumerator StartActCoroutine()
	{
		yield return new WaitForSeconds(2f);
		Fade (1f, 0.5f);
		actImage.DOFade (0f, 0.5f);
		StartAct();
	}

	public void SetPreaches(List<Preach> preachList)
	{
		preaches = preachList;
		selectedPreach = -1;

		SetPreachStates ();
		SetPreachButtons ();
	}

	private void SetPreachStates()
	{
		preachState = new int[preaches.Count];
		for (int i = 0; i < preachState.Length; i++)
		{
			preachState [i] = 0;
		}
	}

	private void SetPreachButtons()
	{
		for (int i = 0; (i < preachButtons.Count) && (i < preaches.Count); i++)
		{
			preachButtons [i].gameObject.SetActive (true);
			preachButtons [i].index = i;
			preachButtons [i].childImage.sprite = preaches [i].preacher.sprite;
			preachButtons[i].miracle.SetActive(false);
			preachButtons[i].deniedIcon.SetActive(false);
		}

		if (preachButtons.Count > preaches.Count)
		{
			for (int i = preaches.Count; i < preachButtons.Count; i++)
			{
				preachButtons [i].gameObject.SetActive (false);
			}
		}
	}

	public void SelectPreach(int index)
	{
		index %= preaches.Count;

		for (int i = 0; i < preachButtons.Count; i++)
		{
			if (i != index)
			{
				preachButtons[i].Unselect();
			}
		}

		if (index >= preaches.Count) {
			return;
		}
		Debug.Log (index);

		selectedPreach = index;
		preachButtons [index].Select ();

		vortexImage.FindChild (preaches [index].preacher.name).SetAsLastSibling ();
		text.text = preaches [selectedPreach].text;

		conceedButton.gameObject.SetActive(preaches[index].eventToChange != -1);
		denyButton.gameObject.SetActive(preaches[index].eventToChange != -1);
		receivedButton.gameObject.SetActive(preaches[index].eventToChange == -1);
		vortexMiracle.SetActive(preachState[index] == 1);

		SetButtonsState ();

		SoundManager.instance.EnterTrack (preaches [selectedPreach].isTense, preaches [selectedPreach].preacher.soundtrackIndex);
	}

	private void SetButtonsState()
	{
		if (preachState [selectedPreach] == 1)
		{
			conceedButton.ChangeState (JudgementButton.ButtonState.clicked);
			denyButton.ChangeState (JudgementButton.ButtonState.normal);
		}
		else if (preachState [selectedPreach] == 0)
		{
			conceedButton.ChangeState (JudgementButton.ButtonState.normal);
			denyButton.ChangeState (JudgementButton.ButtonState.normal);
		}
		else
		{
			conceedButton.ChangeState (JudgementButton.ButtonState.normal);
			denyButton.ChangeState (JudgementButton.ButtonState.clicked);
		}

		receivedButton.ChangeState (JudgementButton.ButtonState.normal);
	}

	public void ConceedButtonClicked()
	{
		if (availableMiracles > 0)
		{
			RemoveMiracle();
			preachState [selectedPreach] = 1;
			preachButtons[selectedPreach].miracle.SetActive(true);
			preachButtons[selectedPreach].deniedIcon.SetActive(false);
			vortexMiracle.SetActive(true);
			ProcessJudgement ();
		}
	}

	public void DenyButtonClicked()
	{
		if (preachState[selectedPreach] == 1)
		{
			AddMiracle();
		}
		preachState [selectedPreach] = -1;
		preachButtons[selectedPreach].miracle.SetActive(false);
		preachButtons[selectedPreach].deniedIcon.SetActive(true);
		vortexMiracle.SetActive(false);
		ProcessJudgement ();
	}

	public void ConfirmButtonClicked()
	{
		TransitionAct ();
	}

	private void ProcessJudgement()
	{
		if (CheckAllPreachesSelected ())
		{
			SetButtonsState ();
			confirmButton.gameObject.SetActive(true);
			confirmButton.ChangeState (JudgementButton.ButtonState.normal);
		}
		else
		{
			SelectNextPendingPreach ();
		}
	}

	private bool CheckAllPreachesSelected()
	{
		for (int i = 0; i < preachState.Length; i++)
		{
			if (preachState [i] == 0)
				return false;
		}
		return true;
	}

	private void SelectNextPendingPreach()
	{
		for (int i = 1; i < preaches.Count; i++)
		{
			int index = (selectedPreach + i) % preaches.Count;
			if (preachState [index] == 0)
			{
				SelectPreach (index);
				return;
			}
		}
		SetButtonsState ();
	}

	private void RemoveMiracle()
	{
		miracles[availableMiracles-1].GetComponent<MiracleMover>().FadeOut();
		availableMiracles--;
	}

	private void AddMiracle()
	{
		availableMiracles++;
		miracles[availableMiracles-1].GetComponent<MiracleMover>().FadeIn();
	}
}