﻿using UnityEngine;
using System.Collections;

public class PowersManager : MonoBehaviour 
{
	public static int currentFaith;
	public static int remainingMiracles;

	public static void IncreaseFaith(int faith)
	{
		currentFaith += faith;
	}

	public static void DecreaseFaith(int faith)
	{
		currentFaith -= faith;
	}
}