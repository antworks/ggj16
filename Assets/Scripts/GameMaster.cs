﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class GameMaster : MonoBehaviour 
{
	public const int NUM_OF_EVENTS = 9;
	public const int NUM_OF_ACTS = 4;

	public static int[] events;

	public int[] publicEvents;

	private void Update()
	{
		for (int i = 0; i < NUM_OF_EVENTS; i++)
		{
			events = publicEvents;
		}
	}
}