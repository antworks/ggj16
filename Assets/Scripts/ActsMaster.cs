using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActsMaster : MonoBehaviour 
{
	public const string ACTS_PATH = "Acts/";
	public static ActsMaster instance;

	public static int currentAct;
	public Act[] acts = new Act[GameMaster.NUM_OF_ACTS];


	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(gameObject);
			return;
		}
	}


	[ContextMenu("LoadActs")]
	private void LoadActs()
	{
		acts = new Act[GameMaster.NUM_OF_ACTS];
		TextAsset[] actsFiles = Resources.LoadAll<TextAsset>(ACTS_PATH);
		for (int i = 0; i < GameMaster.NUM_OF_ACTS; i++)
		{
			acts[i] = LoadSingleAct(actsFiles[i]);
		}
	}

	[ContextMenu("GetCurrentPreaches")]
	public static List<Preach> GetCurrentPreaches()
	{
		List<Preach> currentPreaches = instance.acts[currentAct].GetCurrentPreaches();

		string output = "";
		for (int i = 0; i < currentPreaches.Count; i++)
		{
			output += currentPreaches[i].id + "; ";
		}

		Debug.Log(output);

		return currentPreaches;
	}

	private Act LoadSingleAct(TextAsset actFile)
	{
		Act newAct = new Act();
		PreachMapper newPreachMapper = new PreachMapper();
		string[] fileLines = actFile.text.Split('\n');
		bool onMapper = false;

		for (int i = 0; i < fileLines.Length; i++)
		{
			string line = fileLines[i].Trim();

			if (line == "{")
			{
				if (onMapper)
				{
					Debug.LogError("Found '{' on mapper. Line: " + (i + 1).ToString());
				}
				else
				{
					newPreachMapper = new PreachMapper();
					onMapper = true;
				}
				continue;
			}
			else if (line == "}")
			{
				if (onMapper)
				{
					newAct.preachMapper.Add(newPreachMapper);
					onMapper = false;
				}
				else
				{
					Debug.LogError("Found '}' while not on mapper. Line: " + (i + 1).ToString());
				}
				continue;
			}
			else if (onMapper)
			{
				string[] substrings = line.Split(':');

				switch(substrings[0].Trim())
				{
				case "preachId":
					newPreachMapper.preachId = substrings[1].Trim();
					break;
				case "eventMatch":
					newPreachMapper.neededEvents = ProcessNeededEvents(substrings[1].Trim());
					break;
				default:
					Debug.LogError("WTF?! on line: " + (i + 1).ToString());
					break;
				}
			}
		}

		return newAct;
	}

	private int[] ProcessNeededEvents(string neededEvents)
	{
		int[] mappedNeededEvents = new int[GameMaster.NUM_OF_EVENTS];
		string[] splitNeededEvents = neededEvents.Split(',');

		for (int i = 0; i < GameMaster.NUM_OF_EVENTS; i++)
		{
			mappedNeededEvents[i] = int.Parse(splitNeededEvents[i]);
		}

		return mappedNeededEvents;
	}
}