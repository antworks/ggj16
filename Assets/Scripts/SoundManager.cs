﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour {

	public const float FADE_TIME = 0.5f;
	public const float BASE_VOLUME = 0.575f;

	public static SoundManager instance;

	public AudioSource baseTrack;
	public AudioSource[] joyTracks;
	public float[] joyVolume;
	public AudioSource[] tenseTracks;
	public float[] tenseVolume;

	private int currentTrackIndex;
	private bool currentTrackIsJoy;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(gameObject);
			return;
		}
	}

	[ContextMenu("TesteIn")]
	private void TesteIn()
	{
		EnterTrack (false, 0);
	}

	[ContextMenu("TesteOut")]
	private void TesteOut()
	{
		ExitCurrentTrack ();
	}

	private void Start()
	{
		StartAllTracks ();
	}

	private void StartAllTracks()
	{
		baseTrack.volume = 1;
		baseTrack.Play ();

		for (int i = 0; i < joyTracks.Length; i++)
		{
			joyTracks [i].volume = 0;
			joyTracks [i].Play ();

			tenseTracks [i].volume = 0;
			tenseTracks [i].Play ();
		}

		currentTrackIndex = -1;
	}

	public void EnterTrack(bool isJoy, int trackIndex)
	{
		if (currentTrackIndex != -1)
		{
			SmoothVolumeTo (0f, currentTrackIsJoy, currentTrackIndex);
		}

		if (trackIndex == -1)
		{
			currentTrackIndex = -1;
			return;
		}

		currentTrackIndex = trackIndex;
		currentTrackIsJoy = isJoy;
		SmoothVolumeTo (1f, currentTrackIsJoy, currentTrackIndex);
		SmoothBaseTo (BASE_VOLUME);
	}

	public void ExitCurrentTrack()
	{
		if (currentTrackIndex != -1)
		{
			SmoothVolumeTo (0f, currentTrackIsJoy, currentTrackIndex);
			SmoothBaseTo (1f);
		}
	}

	private void SmoothVolumeTo(float targetVolume, bool isJoy, int trackIndex)
	{
		if (isJoy)
		{
			targetVolume = (targetVolume > 0) ? joyVolume [trackIndex] : 0f;
			joyTracks [trackIndex].DOFade (targetVolume, FADE_TIME);
		}
		else
		{
			targetVolume = (targetVolume > 0) ? tenseVolume [trackIndex] : 0f;
			tenseTracks [trackIndex].DOFade (targetVolume, FADE_TIME);
		}
	}

	private void SmoothBaseTo(float targetVolume)
	{
		baseTrack.DOFade (targetVolume, FADE_TIME);
	}
}