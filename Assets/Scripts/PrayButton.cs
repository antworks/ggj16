﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class PrayButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
	public const float SELECTED_SCALE = 1.1f;

	public int index;
	public Image childImage;
	public GameObject miracle;
	public GameObject deniedIcon;
	private RectTransform rect = null;

	public void OnPointerDown(PointerEventData eventData)
	{

	}

	public void OnPointerUp(PointerEventData eventData)
	{

	}

	public void OnPointerClick(PointerEventData eventData)
	{
		PrayLevel.Instance.SelectPreach (index);
	}

	public void Select()
	{
		if (rect == null)
		{
			rect = GetComponent<RectTransform> ();
		}

		rect.localScale = Vector3.one * SELECTED_SCALE;
	}

	public void Unselect()
	{
		if (rect == null)
		{
			rect = GetComponent<RectTransform> ();
		}

		rect.localScale = Vector3.one / SELECTED_SCALE;
	}
}
