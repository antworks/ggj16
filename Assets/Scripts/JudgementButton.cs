﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class JudgementButton : MonoBehaviour,
	IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
{
	public enum ButtonState { normal, over, pressed, disabled, clicked }
	private ButtonState currentState = ButtonState.normal;

	private Image image;

	public Sprite highlight, mouseDown, disabled;

	public bool isConceed = true;
	public bool isRealize = false;

	private void Awake()
	{
		image = GetComponent<Image> ();
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		if (currentState == ButtonState.normal)
		{
			ChangeState (ButtonState.over);
		}
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		if (currentState == ButtonState.over)
		{
			ChangeState (ButtonState.normal);
		}
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		ChangeState (ButtonState.pressed);
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		if (currentState == ButtonState.pressed)
		{
			if (isConceed && PrayLevel.Instance.availableMiracles > 0)
			{
				ChangeState (ButtonState.clicked);
			}
			else
			{
				ChangeState(ButtonState.over);
			}
			Click ();
		}
	}

	private void Click()
	{
		if (isRealize)
		{
			PrayLevel.Instance.ConfirmButtonClicked ();
			return;
		}

		if (isConceed)
		{
			PrayLevel.Instance.ConceedButtonClicked ();
		}
		else
		{
			PrayLevel.Instance.DenyButtonClicked ();
		}
	}

	public void ChangeState(ButtonState newState)
	{
		switch (newState)
		{
		case ButtonState.over:
			image.overrideSprite = highlight;
			break;
		case ButtonState.clicked:
			image.overrideSprite = highlight;
			break;
		case ButtonState.pressed:
			image.overrideSprite = mouseDown;
			break;
		case ButtonState.disabled:
			image.overrideSprite = disabled;
			break;
		case ButtonState.normal:
		default:
			image.overrideSprite = null;
			break;
		}
		currentState = newState;
	}
}
