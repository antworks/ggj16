﻿using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Preach
{
	public string id;
	public Preacher preacher;
	[TextArea(5, 20)]
	public string text;
	public bool isTense = false;
	public int eventToChange = -1;
}