﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class MiracleSpinner : MonoBehaviour 
{
	public float rotationSpeed;

	void Update () 
	{
		transform.Rotate(Vector3.forward * Time.deltaTime * rotationSpeed);
	}
}