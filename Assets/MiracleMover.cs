﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class MiracleMover : MonoBehaviour {

	private void Start()
	{
		transform.DOMove(transform.position + Vector3.up * 0.8f, 2f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine).SetDelay(Random.Range(0f, 0.5f));
	}

	public void FadeOut()
	{
		gameObject.GetComponent<SpriteRenderer>().DOFade(0f, 1f).SetId("Fade");
	}

	public void FadeIn()
	{
		gameObject.GetComponent<SpriteRenderer>().DOFade(1f, 1f).SetId("Fade");
	}
}
